/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.satit.album_project.service;

import com.satit.album_project.dao.SaleDao;
import com.satit.album_project.model.ReportSale;
import java.util.List;

/**
 *
 * @author satit
 */
public class ReportService {
     public List<ReportSale> getReportSaleByDay(){
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
        
    }
    public List<ReportSale> getReportSaleByMonth(int year){
        SaleDao dao = new SaleDao();
       return dao.getMonthReport(year);
        
    }
}
